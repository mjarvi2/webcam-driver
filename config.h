/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* V4L2 is installed */
#define HAVE_V4L2 1

/* Name of package */
#define PACKAGE "huawei_capture"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "mjarvi2@students.towson.edu"

/* Define to the full name of this package. */
#define PACKAGE_NAME "huawei_capture"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "huawei_capture 002"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "huawei_capture"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "002"

/* Version number of package */
#define VERSION "002"
