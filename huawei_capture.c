//data structure and huawei_webcam_capture from https://github.com/fsphil/fswebcam/blob/master/fswebcam.c

#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <gd.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "huawei_capture.h"
#include "config.h"
#include "log.h"
#include "src.h"
#include "jpeg.h"
#include "parse.h"


typedef struct {	
  char *opts;
  const struct option *long_opts;	
  int opt_index;
	
} huawei_options;

typedef struct {
  uint16_t id;
  char    *options;
} huawei_job;

typedef struct {
  time_t start;
  char *device;
  char *input;
  unsigned long timeout;
  int width;
  int height;
  unsigned int frames;
  unsigned int skipframes;
  int palette;
  src_option_t **option;
  uint8_t jobs;
  huawei_job **job;
  char *filename;
  char compression;
	
}huawei_webcam_config;

int *get_timestamp(char *dst, size_t max, char *src,
		   time_t timestamp){
  struct tm tm_timestamp;	
  // Set the time structure. 
  localtime_r(&timestamp, &tm_timestamp);
	
  // Create the string 
  strftime(dst, max, src, &tm_timestamp);
  return 0;
}

int webcam_output(huawei_webcam_config *config, char *name, gdImage *image){
  char filename[FILENAME_MAX];
  gdImage *im;
  FILE *f;
  //set time
  get_timestamp(filename, FILENAME_MAX, name,config->start);
  //set file name
  f = fopen(filename, "wb");
  gdImageJpeg(image, f, config->compression);		
  return(0);
}

int huawei_webcam_capture(huawei_webcam_config *config){
	uint32_t frame;
	uint32_t x, y;
	avgbmp_t *abitmap, *pbitmap;
	gdImage *image;
	uint8_t modified;
	src_t src;
	int color;
	char *options;
	// Record start time
	config->start = time(NULL);
	// Set source 
	memset(&src, 0, sizeof(src));
	src.timeout = config->timeout; 
	src.palette = config->palette;
	src.width = config->width;
	src.height = config->height;
	src.option = config->option;
	//open the capture card from src
	src_open(&src, config->device);
	
	//allocate memory for bitmap buffer
	abitmap = calloc(config->width * config->height * 3, sizeof(avgbmp_t));
	// grab the frames from jpeg
	for(frame = 0; frame < config->frames; frame++){
	  if(src_grab(&src) == -1) break;
	  capture_jpeg(&src, abitmap);
	}
	
	//close the capture card from src
	src_close(&src);
	
	//copy bitmap image to gdimage dependency
	image = gdImageCreateTrueColor(config->width, config->height);	
	pbitmap = abitmap;
	for(y = 0; y < config->height; y++)
	  for(x = 0; x < config->width; x++){
	    //set the correct colors so it doesn't look like garbage
	      color  = (*(pbitmap++) / config->frames) << 16;
	      color += (*(pbitmap++) / config->frames) << 8;
	      color += (*(pbitmap++) / config->frames);
	      gdImageSetPixel(image, x, y, color);
	  }
	
	//set defualt values and free memory
	free(abitmap);
	if(config->filename) free(config->filename);
	config->filename     = NULL;
	config->compression  = -1;	
	//output the image from this file
	for(x = 0; x < config->jobs; x++){	
	    webcam_output(config, config->job[x]->options, image);
	}
	gdImageDestroy(image);	
	return(0);
}

int huawei_webcam_add_job(huawei_webcam_config *config, uint16_t id, char *options){
	huawei_job *job;
	void *n;
	job = malloc(sizeof(huawei_job));	
	job->id = id;
	job->options = strdup(options);
	//increase the job queue size
	n = realloc(config->job, sizeof(huawei_job *) * (config->jobs + 1));	
	config->job = n;
	//add new job
	config->job[config->jobs++] = job;
	return(0);
}

int huawei_webcam_get_job(huawei_options *s, int argc, char *argv[]){
      	//read from command line and send to parse file
       return  getopt_long(argc, argv, s->opts, s->long_opts, &s->opt_index);
}

int huawei_webcam_args(huawei_webcam_config *config, int argc, char *argv[]){
  int c;
  huawei_options s;
  char *opts = "-";	
  s.opts      = opts;
  s.opt_index = 0;
	
  /* Set the defaults. */
  config->device = strdup("/dev/video0");
  config->timeout = 10;
  config->width = 640;
  config->height = 360;
  config->frames = 1;
  config->palette = SRC_PAL_ANY;
  config->jobs = 0;
	
  //get file
  while((c = huawei_webcam_get_job(&s, argc, argv)) != -1){
    huawei_webcam_add_job(config, c, optarg);
    }
  return(0);
}

int main(int argc, char *argv[]){
  
  huawei_webcam_config *config;
  int i = 0;

  //allocate memory for struct
  config = calloc(sizeof(huawei_webcam_config), 1);
  
  //call huawei_webcam_args to process the command line argument(s)
  huawei_webcam_args(config, argc, argv);	

  //take picture
   i = huawei_webcam_capture(config);
  //end main
  return(i);
}
