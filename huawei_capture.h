//header file for huwei_capture.c

#include <stdint.h>
#include "config.h"

typedef uint16_t avgbmp_t;
#define MAX_FRAMES (UINT16_MAX >> 8)
#define CLIP(val, min, max) (((val) > (max)) ? (max) : (((val) < (min)) ? (min) : (val)))

