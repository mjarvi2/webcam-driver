//functions from this file are from https://github.com/fsphil/fswebcam/blob/master/parse.c

/* Copyright (C)2005-2011 Philip Heron <phil@sanslogic.co.uk> */
/*                                                            */
/* This program is distributed under the terms of the GNU     */
/* General Public License, version 2. You may use, modify,    */
/* and redistribute it under the terms of this license. A     */
/* copy should be included with this source.                  */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "parse.h"

#define ARG_EOS (-1) // End of string   
#define ARG_EOA (-2) // End of argument

typedef struct {
	char *s;
	char *sep;
	char opts;
	char quote;
	
} arg_t;

int arggetc(arg_t *arg){
	if(*arg->s == '\0') return(ARG_EOS);
	
	if((!(arg->opts & ARG_NO_ESCAPE) && *arg->s == '\\') &&
	    !(arg->quote && *(arg->s + 1) != '"')){
		if(*(++arg->s) == '\0') return(ARG_EOS);
		return((int) *(arg->s++));
	}
	
	if(!(arg->opts & ARG_NO_QUOTE) && *arg->s == '"'){
		arg->s++;
		arg->quote = !arg->quote;
		
		return(arggetc(arg));
	}
	
	if(arg->quote) return((int) *(arg->s++));
	
	if(strchr(arg->sep, *arg->s))
	{
		arg->s++;
		return(ARG_EOA);
	}
	
	return((int) *(arg->s++));
	}

int argopts(arg_t *arg, char *src, char *sep, int opts){
	memset(arg, 0, sizeof(arg_t));
	arg->s    = src;
	arg->sep  = sep;
	arg->opts = opts;
	return(0);
}

int argncpy(char *dst, size_t n, char *src, char *sep, int arg, int opt){
	arg_t s;
	char *d;
	size_t l;
	int r;
	argopts(&s, src, sep, opt);
	d = dst;
	*d = '\0';
	l = 0;
	while((r = arggetc(&s)) != ARG_EOS){
		if(r == ARG_EOA){
			if(!(opt & ARG_NO_TRIM) && !l) continue;
			if(!arg--) return(0);
			d = dst;
			*d = '\0';
			l = 0;
			continue;
		}
		if(n > l){
			*(d++) = r;
			*d = '\0';
			l++;
		}
	}
	if(!arg && !(!(opt & ARG_NO_TRIM) && !l)) return(0);
	return(-1);
	}

