//Header file for parse.c

#ifndef INC_PARSE_H
#define INC_PARSE_H

#define ARG_NONE      (0)
#define ARG_NO_QUOTE  (1 << 1)
#define ARG_NO_ESCAPE (1 << 2)
#define ARG_NO_TRIM   (1 << 3)

#define WHITESPACE " \t\n\r"
extern int argncpy(char *dst, size_t n, char *src, char *sep, int arg, int opt);
extern int arglen(char *src, char *sep, int arg, int opt);

extern char *argdup(char *src, char *sep, int arg, int opt);

#endif

